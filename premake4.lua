function defaultaction(osName, actionName)
   if os.is(osName) then
      _ACTION = _ACTION or actionName
   end
end

solution "trumpet"
    defaultaction ("windows", "vs2010")
    defaultaction ("linux", "gmake")
    
    configurations { "Debug", "Release"}

    project "trumpet"
        language "C++"
        files { "src/**.hpp", "src/**.h", "src/**.cpp", "src/**.c" }
        flags { "FloatFast" }

		if os.get() == "windows" then
			defines { "WIN32" }
            links { "opengl32", "glu32", "glew32", "SDL", "SDLMain", "wsock32" }
		
            includedirs { "include" }
            libdirs { "lib" }
        else
            buildoptions { "-std=c++11 -Wall" }
            links { "GL", "GLU", "GLEW", "SDL"}
        end
        
        
        configuration "Debug"
            kind "ConsoleApp"
            defines { "DEBUG" }
            flags { "Symbols" }
			
        configuration "Release"
            kind "WindowedApp"
            defines { "NDEBUG" }
            flags { "OptimizeSpeed" }
			
            
