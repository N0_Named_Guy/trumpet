#include "World.hpp"

World::World(void)
{
	nextWorld = NULL;
	running = true;
}

World::~World(void) {}

void World::setup(void) {}
void World::processInput(float dt) {}
void World::processLogic(float dt) {}
void World::renderScene(void) {}
void World::cleanup(void) {}
