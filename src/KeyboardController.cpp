#include "KeyboardController.hpp"

void KeyboardController::clean(void) {
	if (nActions) {
		delete[] controllerKeys;
		delete[] actions;
	}
}


void KeyboardController::init() {
	SDLKey k[] = CONTROLLER_KEYS;
	nActions = sizeof(k) / sizeof(SDLKey);
	
	controllerKeys = new SDLKey[nActions];
	actions = new bool[nActions];

	for (int i = 0; i < nActions; i++) {
		actions[i] = false;
		controllerKeys[i] = k[i];
	}
}

void KeyboardController::loadControls(Json::Value& node) {
	init();

	unsigned int n = node.size();
	ControllerActions curAction;
	Json::Value::Members strActions = node.getMemberNames();

	for (unsigned int i = 0; i < n; i++) {
		curAction = (ControllerActions)getActionFromString(strActions[i]);
		
		controllerKeys[curAction] =
			(SDLKey)node.get(strActions[i], controllerKeys[curAction]).asInt();
	}

}

void KeyboardController::poll() {
	SDL_Event event;
		
	while (SDL_PollEvent(&event)) {
		if (event.type == SDL_KEYDOWN) {
			for (int i = 0; i < nActions; i++) {
				if (event.key.keysym.sym == controllerKeys[i]) {
					actions[i] = true;
				}
			}
		
		} else if (event.type == SDL_KEYUP) {
			for (int i = 0; i < nActions; i++) {
				if (event.key.keysym.sym == controllerKeys[i]) {
					actions[i] = false;
				}
			}
		
		} else if (event.type == SDL_MOUSEMOTION) {
			axis.x = event.motion.xrel;
			axis.y = event.motion.yrel;
		
		} else if (event.type == SDL_QUIT) {
			actions[quit] = true;
		}
	}
}
