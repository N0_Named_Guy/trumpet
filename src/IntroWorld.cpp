#include "IntroWorld.hpp"

extern Config cfg;
extern ShaderManager shaderManager;
extern SurfaceManager surfaceManager;

IntroWorld::IntroWorld(std::string startMap) {
	surf = surfaceManager.getSurface("intro.png");
	surf.sy = 0.5f;
	surf.x = 0.5f;
	surf.y = 0.5f;
	gameWorld = startMap;
	nextWorld = NULL;
}


void IntroWorld::cleanup(void) {

}

void IntroWorld::setup(void) {
	if (cfg.screenW > cfg.screenH) {
		glViewport((cfg.screenW - cfg.screenH) / 2, 0, cfg.screenH, cfg.screenH);
	} else {
		glViewport(0, (cfg.screenH - cfg.screenW) / 2, cfg.screenW, cfg.screenW);
	}
	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    projection = glm::ortho(0.0f, 1.0f, 0.0f, 1.0f);

	surf.fadeIn(500);
}

void IntroWorld::processLogic(float dt) {
	if (surf.animating == false) {
		if (nextWorld == NULL) {
			nextWorld = new LocalGameWorld();
            ((GameWorld*)nextWorld)->loadWorld(gameWorld);
			
            //nextWorld = new NetServerWorld();
            //((NetServerWorld*)nextWorld)->loadWorld(gameWorld);

			//nextWorld = new PlayerSelect(gameWorld);
			
			d("%s\n", "Done loading");
			surf.fadeOut(500);
		} else {
			running = false;
		}
	}
}

void IntroWorld::renderScene(void) {	
	glClear(GL_COLOR_BUFFER_BIT);
	
	surf.render(projection);
	SDL_GL_SwapBuffers();
}
