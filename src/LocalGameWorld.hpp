#ifndef POTATO_LOCAL_GAME_WORLD_H
#define POTATO_LOCAL_GAME_WORLD_H
#pragma once

#include "GameWorld.hpp"
#include "HUD.hpp"
#include "Player.hpp"

class LocalGameWorld :
	public GameWorld
{
protected:
	HUD hud;
	HUDData data;

	Player player;
	bool firstPerson;

	virtual void processPlayer(float dt);
public:
	LocalGameWorld(void);

	virtual void setup(void);
	virtual void cleanup(void);

	virtual void loadWorld(const std::string& worldFile);

	virtual void processInput(float dt);
	virtual void processLogic(float dt);
	virtual void renderScene(void);
};

#endif /* POTATO_LOCAL_GAME_WORLD_H */
