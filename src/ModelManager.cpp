#include "ModelManager.hpp"

Model* ModelManager::getOBJModel(std::string filename) {
	Model* m = pointers[filename];
	if (!m) {
		m = new OBJModel();
		d("<%s> Loading\n", filename.c_str());
		((OBJModel*)m)->loadFile(filename);
		pointers[filename] = m;
	}
	
	return m;
}

void ModelManager::freeAllModels() {
	for (std::map<std::string, Model*>::iterator it = pointers.begin(); it != pointers.end(); it++) {
		Model* m = (*it).second;
		m->clean();
		delete m;
	}
}
