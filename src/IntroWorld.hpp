#ifndef POTATO_INTRO_WORLD_H
#define POTATO_INTRO_WORLD_H

#pragma once
#include "World.hpp"
#include "SurfaceManager.hpp"
#include "GLSurface.hpp"
#include "GameWorld.hpp"
#include "LocalGameWorld.hpp"
#include "NetServerWorld.hpp"
#include "ShaderManager.hpp"

class IntroWorld :
	public World
{
protected:
	GLuint texCredits;
	Scene scene;
	GLSurface surf;
	std::string gameWorld;

public:
	IntroWorld(std::string startMap);
	
	virtual void setup(void);
	virtual void cleanup(void);
	virtual void processLogic(float interpolation);
	virtual void renderScene(void);
};
#endif /* POTATO_INTRO_WORLD_H */
