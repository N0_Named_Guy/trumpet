#include "Camera.hpp"

Camera::Camera(void) {
	follow = NULL;
	mode = FREE;
}

Camera::~Camera(void)
{
}

glm::mat4 Camera::getView(void) {
	return getView(true);
}

glm::mat4 Camera::getView(const bool translate) {
	glm::mat4 transf;

	switch (mode) {
	case FIRST_PERSON:
		x = follow->x;
		y = follow->y;
		z = follow->z;
		
		yaw = follow->yaw;
		pitch = follow->pitch;
		roll = follow->roll;

        transf = glm::rotate(transf, -pitch, glm::vec3(1.0f, 0.0f, 0.0f));
        transf = glm::rotate(transf, -yaw, glm::vec3(0.0f, 1.0f, 0.0f));
        transf = glm::rotate(transf, -roll, glm::vec3(0.0f, 0.0f, 1.0f));

		if (translate) {
			transf = glm::translate(transf, glm::vec3(-x, -y, -z));
		}
		break;

	case FREE:
		transf = glm::rotate(transf, -pitch, glm::vec3(1.0f, 0.0f, 0.0f));
		transf = glm::rotate(transf, -yaw, glm::vec3(0.0f, 1.0f, 0.0f));
		transf = glm::rotate(transf, -roll, glm::vec3(0.0f, 0.0f, 1.0f));

		if (translate) {
			transf = glm::translate(transf, glm::vec3(-x,-y,-z));
		}
		break;

	case THIRD_PERSON:
        if (lockedRotationEnabled) {
            x = followDist * cos((lockedRotation + 270.0f) * PIOVER180) + follow->x;
            z = followDist * sin((lockedRotation - 270.0f) * PIOVER180) + follow->z;
        } else { 
            x = followDist * cos((follow->yaw + 270.0f) * PIOVER180) + follow->x;
            z = followDist * sin((follow->yaw - 270.0f) * PIOVER180) + follow->z;
        }

		if (translate) {
			transf = glm::lookAt(
				glm::vec3(x,y,z),
				glm::vec3(follow->x, follow->y, follow->z),
				glm::vec3(0.0f, 1.0f, 0.0f)
			);
		} else {
			transf = glm::lookAt(
				glm::vec3(0,0,0),
				glm::vec3(follow->x - x, follow->y - y, follow->z - z),
				glm::vec3(0.0f, 1.0f, 0.0f)
			);
		}
		break;
	}

	return transf;
}


void Camera::firstPerson(const Entity& ent) {
	follow = &ent;
	followDist = 0.0f;
	mode = FIRST_PERSON;
}

void Camera::thirdPerson(const Entity& ent, float distance, float locked) {
	follow = &ent;
	followDist = distance;
    lockedRotation = locked;
    lockedRotationEnabled = true;
	mode = THIRD_PERSON;
}

void Camera::thirdPerson(const Entity& ent, float distance) {
    thirdPerson(ent, distance, 0.0f);
    lockedRotationEnabled = false;
}

void Camera::freeCamera() {
	this->follow = NULL;
	mode = FREE;
}
