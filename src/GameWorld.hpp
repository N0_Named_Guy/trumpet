#ifndef POTATO_GAME_WORLD_H
#define POTATO_GAME_WORLD_H
#pragma once

#include "World.hpp"
#include "Scene.hpp"

class Unit;
#include "Unit.hpp"

#include "ModelEntity.hpp"
#include "Controller.hpp"

class GameWorld : public World
{
protected:
	Scene scene;

	std::string curWorldFile;

    int mapWidth, mapHeight;
    float tileWidth, tileHeight;
    int* tileMap;
    Model* tileSet;
    int nTileset;
	
	virtual void spawnUnit(Unit& spwaner);

	void parseModel(Json::Value& modelNode, ModelEntity& entity);
	void parseCheckpoints(Json::Value& checkpNode);
	void parseScenery(Json::Value& sceneryNode);
    void parseTilemap(Json::Value& tilemapNode);
	
	virtual void loadWorld(const Json::Value& root);
    virtual void controlUnit(Unit& unit, Controller& control, float dt);
;

public:
	Controller* control;

	Terrain terrain;
	unsigned int nCheckpoints;
	ModelEntity* checkpoints;
	unsigned int goalCheckpoint;

	unsigned int nScenery;
	ModelEntity* scenery;

	std::list<Unit*> units;

	GameWorld(void);

	virtual void setup(void);
	virtual void cleanup(void);

	virtual void loadWorld(const std::string& worldFile);

	virtual void clearWorld(void);
	virtual void reloadWorld(void);

	virtual void processInput(float dt);
	virtual void processLogic(float dt);
	virtual void renderScene(void);
};

#endif /* POTATO_GAME_WORLD_H */
