#include "ModelEntity.hpp"

ModelEntity::ModelEntity(Model* model) {
	modelPointer = model;
}

void ModelEntity::render(const glm::mat4& proj, const glm::mat4& view) {
    glm::mat4 model;

    model = glm::translate(model, glm::vec3(x, y, z));

    model = glm::rotate(model, yaw, glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::rotate(model, pitch, glm::vec3(1.0f, 0.0f, 0.0f));
    model = glm::rotate(model, roll, glm::vec3(0.0f, 0.0f, 1.0f));

    model = glm::scale(model, glm::vec3(sx, sy, sz));

	if (modelPointer)
		modelPointer->render(proj, view, model);
}

bool ModelEntity::collide(ModelEntity& ent) {
	return collide(ent, ent.x, ent.y, ent.z);
}

bool ModelEntity::collide(ModelEntity& ent, float nx, float ny, float nz) {
	Model& m1 = *modelPointer;
	Model& m2 = *ent.modelPointer;

	BoundingBox gAABB1 = m1.aabb;
	BoundingBox gAABB2 = m2.aabb;

	// Calculate AABB in global coordinates
	gAABB1.maxX += x; gAABB1.maxY += y; gAABB1.maxZ += z;
	gAABB1.minX += x; gAABB1.minY += y; gAABB1.minZ += z;

	gAABB2.maxX += nx; gAABB2.maxY += ny; gAABB2.maxZ += nz;
	gAABB2.minX += nx; gAABB2.minY += ny; gAABB2.minZ += nz;
	
	// Some aliasing won't hurt
	float ax1 = gAABB1.minX * sx, ay1 = gAABB1.minY * sy, az1 = gAABB1.minZ * sz;
	float ax2 = gAABB1.maxX * sx, ay2 = gAABB1.maxY * sy, az2 = gAABB1.maxZ * sz;
	float bx1 = gAABB2.minX * sx, by1 = gAABB2.minY * sy, bz1 = gAABB2.minZ * sz;
	float bx2 = gAABB2.maxX * sx, by2 = gAABB2.maxY * sy, bz2 = gAABB2.maxZ * sz;

	return ((ax1 <= bx2) && (ay1 <= by2) && (az1 <= bz2) &&
			(ax2 >= bx1) && (ay2 >= by1) && (az2 >= bz1));
}
