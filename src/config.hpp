#ifndef POTATO_CONFIG_H
#define POTATO_CONFIG_H
#pragma once

#include "stdafx.h"

/// <summary>Default data path</summary>
#define DATA_PATH "data/"

/// <summary>Default models path</summary>
#define MODEL_PATH "data/models/"

/// <summary>Default game textures path</summary>
#define TEXTURE_PATH "data/textures/"

/// <summary>Default texture extension</summary>
#define TEXTURE_EXT ".png"

/// <summary>Default shaders path</summary>
#define SHADER_PATH "data/shaders/"

/// <summary>Default maps path</summary>
#define MAP_PATH "data/maps/"

/// <summary>Default skies path</summary>
#define SKY_PATH "data/sky/"

/// <summary>Default sounds path</summary>
#define SOUND_PATH "data/sounds/"

/// <summary>Default player definitions path</summary>
#define PLAYER_PATH "data/players/"

/// <summary>Default player listing filename</summary>
#define PLAYER_LIST "players.list"

/// <summary>Default configuration filename</summary>
#define CONFIG_PATH "config.json"

/// <summary>Default starting map</summary>
#define START_MAP "raceway.json"

/// <summary>Default player name</summary>
#define PLAYER_NAME "Player"

/// <summary>Default screen width</summary>
#define SCREEN_W 1280

/// <summary>Default screen height</summary>
#define SCREEN_H 600

/// <summary>Default screen mode</summary>
#define FULLSCREEN false

/// <summary>Default show fps setting</summary>
#define SHOW_FPS false

/// <summary>Default game speed</summary>
#define GAME_SPEED 60

/// <summary>Default frames per second setting</summary>
#define FPS 60

/// <summary>Default input latency</summary>
#define INPUT_LATENCY 0

/// <summary>Default net host</summary>
#define NET_HOST "127.0.0.1"

/// <summary>Default net port</summary>
#define NET_PORT 9666

/// <summary>Default net rate</summary>
#define NET_RATE FPS

/// <summary>Default controller keys</summary>
#define CONTROLLER_KEYS { \
	SDLK_LEFT, \
	SDLK_RIGHT, \
	SDLK_DOWN, \
    SDLK_UP, \
	SDLK_s, \
	SDLK_a, \
	SDLK_d, \
	\
    SDLK_RETURN, \
	SDLK_r, \
    \
	SDLK_PLUS, \
    SDLK_MINUS, \
    \
	SDLK_ESCAPE \
}


/// <summary>
/// The game's configuration class.
/// This class contains all the configurations loaded from default values or a
/// configuration file
/// </summary>
class Config {
public:
	/// <summary>The current screen width</summary>
	unsigned int screenW;

	/// <summary>The current screen height</summary>
	unsigned int screenH;

	/// <summary>The current game speed</summary>
	unsigned int gameSpeed;

	/// <summary>The current frames per second setting</summary>
	unsigned int fps;

	/// <summary>The current screen mode</summary>
	bool fullscreen;

	/// <summary>FPS counting</summary>
	bool showFPS;

	/// <summary>
	/// If set, the intro screen is skipped during the game's loading
	/// </summary>
	bool skipIntro;

	/// <summary>The current inital game</summary>
	std::string initialMap;

	/// <summary>The current player nickname</summary>
	std::string playerName;

	/// <summary>The current game controls</summary>
	Json::Value controls;

	/// <summary>The current net host</summary>
	std::string netHost;
	
	/// <summary>The current net host</summary>
	unsigned short netPort;

	/// <summary>The current network update rate</summary>
	unsigned int netRate;

	// Debug
	unsigned int minInputLatency;
	unsigned int maxInputLatency;
	unsigned int dropProbability;

	Config(void);
	~Config(void);
	
	/// <summary>
	/// Loads the game's configuration from the default configuration file
	/// </summary>
	void load(void);
	
	/// <summary>
	/// Loads a specific game configuration file
	/// </summary>
	/// <param name="file">The file's name</param>
	void load(std::string& file);
};

#endif /* POTATO_CONFIG_H */
