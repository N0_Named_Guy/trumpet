#include <iostream>
#include <fstream>
#include "GameWorld.hpp"

#include "ModelManager.hpp"
#include "TextureManager.hpp"

extern ModelManager modelManager;
extern TextureManager textureManager;
extern Config cfg;

GameWorld::GameWorld(void) {
	control = NULL;
	nCheckpoints = 0;
	nScenery = 0;
    nTileset = 0;
    tileMap = NULL;
    tileSet = NULL;
    mapWidth = 0;
    mapHeight = 0;
}

void GameWorld::cleanup(void) {
	clearWorld();
	scene.clean();
	if (control) {
		control->clean();
		delete control;
	}
}

void GameWorld::setup() {}

void GameWorld::parseModel(Json::Value& modelNode, ModelEntity& entity) {
	Model* model = modelManager.getOBJModel(modelNode.get("model","").asString());

	entity.modelPointer = model;
	entity.x = (float)modelNode.get("x", 0.0).asDouble();
	entity.y = (float)modelNode.get("y", 0.0).asDouble();
	entity.z = (float)modelNode.get("z", 0.0).asDouble();
	
	entity.pitch = (float)modelNode.get("pitch", 0.0).asDouble();
	entity.yaw = (float)modelNode.get("yaw", 0.0).asDouble();
	entity.roll = (float)modelNode.get("roll", 0.0).asDouble();

	entity.sx = (float)modelNode.get("sx", 1.0).asDouble();
	entity.sy = (float)modelNode.get("sy", 1.0).asDouble();
	entity.sz = (float)modelNode.get("sz", 1.0).asDouble();

	scene.addEntity(entity);

}

void GameWorld::parseScenery(Json::Value& sceneryNode) {
	nScenery = sceneryNode.size();
	scenery = new ModelEntity[nScenery];
	
	for (unsigned int i = 0; i < nScenery; i++) {
		scenery[i] = ModelEntity();
		parseModel(sceneryNode[i], scenery[i]);
	}
}

void GameWorld::parseCheckpoints(Json::Value& checkpNodes) {
	nCheckpoints = checkpNodes.size();
	checkpoints = new ModelEntity[nCheckpoints];
	
	for (unsigned int i = 0; i < nCheckpoints; i++) {
		checkpoints[i] = ModelEntity();
		parseModel(checkpNodes[i], checkpoints[i]);
		checkpoints[i].y = terrain.getHeight(checkpoints[i].x, checkpoints[i].z);

		if (checkpNodes[i].get("isgoal", false).asBool()) {
			goalCheckpoint = i;
		}
	}
}

void GameWorld::parseTilemap(Json::Value& tilemapNode) {
    mapWidth = tilemapNode.get("width", 1).asInt();
    mapHeight = tilemapNode.get("height", 1).asInt();

    tileMap = new int[(mapWidth * mapHeight)];
}

void GameWorld::clearWorld() {
	if (nCheckpoints) {
		delete[] checkpoints;
		nCheckpoints = 0;
	}

	if (nScenery) {
		delete[] scenery;
		nScenery = 0;
	}

    if (tileMap != NULL) {
        delete[] tileMap;
        mapHeight = mapWidth = 0;
        tileMap = NULL;
    }

    if (tileSet != NULL) {
        delete[] tileSet;
        tileSet = NULL;
        nTileset = 0;
    }

	for (std::list<Unit*>::iterator it = units.begin(); it != units.end(); it++) {
		delete *it;
	}
	units.clear();

}

void GameWorld::reloadWorld() {
	loadWorld(curWorldFile);
}
void GameWorld::loadWorld(const std::string& worldFile) {
	Json::Value root;
	Json::Reader reader;
    std::string worldPath = MAP_PATH + worldFile;

	root = readJSONDoc(worldPath, reader);
	curWorldFile = worldFile;

	loadWorld(root);
}

void GameWorld::loadWorld(const Json::Value& root) {
	clearWorld();
	scene.clean();
	scene = Scene();

	std::string skyboxName = root.get("skybox", "").asString();
	scene.skybox.loadSkybox(skyboxName);

	Json::Value terrainNode = root.get("terrain", "");
	std::string hMap = terrainNode.get("heightMap", "").asString();
	std::string terrainTex = terrainNode.get("texture", "").asString();
	float terrainScale = (float)terrainNode.get("scale", "1.0").asDouble();
	float minH = (float)terrainNode.get("minHeight", "0").asDouble();
	float maxH = (float)terrainNode.get("maxHeight", "100").asDouble();
	int detail = terrainNode.get("detail", "1").asInt();
	
	terrain = Terrain();
	terrain.loadFromPNG(hMap, terrainTex, minH, maxH, terrainScale, detail);
	scene.addEntity(terrain);

    Json::Value checkpNode = root.get("checkpoints", "");
	parseCheckpoints(checkpNode);

    Json::Value sceneryNode = root.get("scenery", "");
	parseScenery(sceneryNode);

    Json::Value tilemapNode = root.get("tilemap", "");
    parseTilemap(tilemapNode);
}

void GameWorld::controlUnit(Unit& unit, Controller& control, float dt) {
    if (control.isDown(left)) {
        unit.setYaw(-90.0f);
        unit.setSpeed(3.0f);
    } else if (control.isDown(right)) {
        unit.setYaw(90.0f);
        unit.setSpeed(3.0f);
    }

    if (control.isPressed(jump)) {
        unit.jump();
    }
}

void GameWorld::processInput(float dt) {}

void GameWorld::processLogic(float dt) {
	
	for (std::list<Unit*>::iterator it = units.begin();
		it != units.end(); it++) {
		(**it).renderLogic(this, dt);
	}

}

void GameWorld::renderScene(void) {}

void GameWorld::spawnUnit(Unit& spawner) {
	Unit* inf = new Unit();
	Model* infModel = modelManager.getOBJModel("sun.obj");
	inf->modelPointer = infModel;

	inf->setLocation(spawner);
	inf->setRotation(spawner);
	inf->setSpeed(fabsf(spawner.maxSpeed) * 2.0f);

	scene.addEntity(*inf);
	units.push_back(inf);
}
