#include "Entity.hpp"
#include "stdafx.h"

Entity::Entity(void) {
	sx = sy = sz = 1.0f;
	x = y = z = 0.0f;
	pitch = yaw = roll = 0.0f;

	roll = 0.0f;
}

Entity::~Entity(void)
{
}

void Entity::turnRight(float angle) {
	//ry -= angle;
	yaw -= angle;
}

void Entity::turnUp(float angle) {
	//rx += angle;
	pitch -= angle;
}

void Entity::forward(float step) {
	//forward(step, ry);
	forward(step, yaw);
}

void Entity::forward(float step, float heading) {
	x += (float)sin(-heading*PIOVER180) * step;			// Move On The X-Plane Based On Player Direction
	z -= (float)cos(-heading*PIOVER180) * step;			// Move On The Z-Plane Based On Player Direction
}

void Entity::strafe(float step) {
	//forward(step, ry - 90.0f);
	forward(step, yaw - 90.0f);
}

void Entity::setLocation(Entity& ent) {
	setLocation(ent.x, ent.y, ent.z);
}

void Entity::setLocation(float x, float y, float z) {
	this->x = x;
	this->y = y;
	this->z = z;
}

void Entity::setRotation(Entity& ent) {
	setRotation(ent.pitch, ent.yaw, ent.roll);
}

void Entity::setRotation(float pitch, float yaw, float roll) {
	this->pitch = pitch;
	this->yaw = yaw;
	this->roll = roll;
}

void Entity::setPitch(float pitch) {
    this->pitch = pitch;
}

void Entity::setYaw(float yaw) {
    this->yaw = yaw;
}

void Entity::setRoll(float roll) {
    this->roll = roll;
}

void Entity::setScale(Entity& ent) {
	setScale(ent.sx, ent.sy, ent.sz);
}

void Entity::setScale(float sx, float sy, float sz) {
	this->sx = sx;
	this->sy = sy;
	this->sz = sz;
}
