#include "SurfaceManager.hpp"
#include "ShaderManager.hpp"
extern ShaderManager shaderManager;

SurfaceManager::SurfaceManager() {
}

void SurfaceManager::clean(void) {
	glDeleteBuffers(1, &vbo);
	glDeleteBuffers(1, &ebo);

	glDeleteVertexArrays(1, &vao);
}

void SurfaceManager::init(void) {
	d("%s\n", "Initializing surface shaders\n");

	// Create Vertex Array Object
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// Create a Vertex Buffer Object containing all the vertex data
	glGenBuffers(1, &vbo);
	GLfloat vertices[] = {
		-0.5f,  0.5f, 0.0f, 0.0f, // Top-left
			0.5f,  0.5f, 1.0f, 0.0f, // Top-right
			0.5f, -0.5f, 1.0f, 1.0f, // Bottom-right
		-0.5f, -0.5f, 0.0f, 1.0f  // Bottom-left
	};

	// Bind the Vertex Buffer Object to the Vertex Array Object
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	// Create an Element Buffer Object containing all the elements to render
	GLuint ebo = 0;
	glGenBuffers(1, &ebo);
	GLuint elements[] = {
		2, 1, 0,
		0, 3, 2
	};

	// Bind the Element Buffer Object to the current Vertex Array Object
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elements), elements, GL_STATIC_DRAW);
		
	// Get the shader program for 2D surfaces
	surfShaderProgram = shaderManager.getProgram("surf.json");

	// Get all the needed uniform references
	colorUnif = glGetUniformLocation(surfShaderProgram, "color");
	projUnif = glGetUniformLocation(surfShaderProgram, "proj");
	modelUnif = glGetUniformLocation(surfShaderProgram, "model");
		
	// Set the layout of the vertex data
	GLuint posAttrib = glGetAttribLocation(surfShaderProgram, "position");
	glEnableVertexAttribArray(posAttrib);
	glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), BUFFER_OFFSET(0,GLfloat));

	GLuint texCoordAttrib = glGetAttribLocation(surfShaderProgram, "texCoord");
	glEnableVertexAttribArray(texCoordAttrib);
	glVertexAttribPointer(texCoordAttrib, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), BUFFER_OFFSET(2,GLfloat));
}

GLSurface SurfaceManager::getSurface(const std::string& filename) {
	return GLSurface(filename, vao, surfShaderProgram, projUnif, modelUnif, colorUnif);
}
