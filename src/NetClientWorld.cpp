#include "NetClientWorld.hpp"
#include "KeyboardController.hpp"

extern Config cfg;
extern SurfaceManager surfaceManager;

NetClientWorld::NetClientWorld(void) {
    status = DISCONNECTED;
}

void NetClientWorld::setup() {
	GameWorld::setup();
	connectSurf = surfaceManager.getSurface("connecting.png");
	connectSurf.sx = 2.0f;
	connectSurf.x = 0.0f;
	connectSurf.y = 0.0f;

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	control = new KeyboardController();
	((KeyboardController*)control)->loadControls(cfg.controls);

	netTimer.reset(1000 / cfg.netRate);
}

void NetClientWorld::setupGraphics(void) {
	glEnable(GL_DEPTH_TEST);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);
	
	glEnable(GL_BLEND);
	
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void NetClientWorld::loadWorld(const std::string& worldFile) {
	Json::Value root;
	Json::Reader reader;
    std::string worldPath = MAP_PATH + worldFile;

	root = readJSONDoc(worldPath, reader);
	curWorldFile = worldFile;
	
	GameWorld::loadWorld(root);

	std::string playerFile = root.get("player", "").asString();
	player->loadPlayer(playerFile);
	player->laps = root.get("laps", 0).asInt();
	scene.addEntity(*player);
	
	player->curCheckp = goalCheckpoint;
	player->setLocation(checkpoints[goalCheckpoint]);
	player->setRotation(checkpoints[goalCheckpoint]);
	
	if (control) {
		delete control;
	}
	control = new KeyboardController();
	((KeyboardController*)control)->loadControls(cfg.controls);

	scene.camera.y = player->y;
	scene.camera.thirdPerson(*player, 10.0f);

	setupGraphics();
}

void NetClientWorld::cleanup() {
    NetCommand cmd(QUIT);
    socket.setNonblock(false);
    socket.send(cmd);
    socket.setNonblock(true);
    d("[CL] %s\n", "disconnected");
	status = DISCONNECTED;
}

void NetClientWorld::processInput(float dt) {
	GameWorld::processInput(dt);
	control->poll();

	if (control->isPressed(quit)) {
		running = false;
	}

	processNetwork(dt);
}

void NetClientWorld::parseCommand(NetCommand& cmd) {
	char id;
	std::string name;
	std::string level;

	NetCommand* toSend = NULL;
    char command = cmd.getCommand();

	switch (command) {
	case ACCEPT:
		name = cmd.getArgument(0);
		id = *(cmd.getArgument(1));

		d("[CL] Accepted! %s [%d]\n", name.c_str(), id);
		status = WAITING;
		
		player = new NetPlayer(name, serverAddr, id);
		break;

	case LEVEL:
		level = cmd.getArgument(0);
		d("[CL] Loading level %s\n", level.c_str());
		loadWorld(std::string(level));

		status = PLAYING;
		toSend = new NetCommand(READY);
		socket.send(*toSend);
		delete toSend;

		break;

	case PING:
		d("[CL] %s\n", "Got ping request");

		toSend = new NetCommand(PONG);
		toSend->addArgument(cmd.getArgument(0));
		socket.send(*toSend);
		delete toSend;

		break;

    case PONG:
        d("[CL] %s\n", "Got a pong response");
        break;

    case QUIT:
        d("[CL] %s\n", "Asked to quit.");
        running = false;
        break;


    case CONNECT:
    case READY:
    case JOIN:
    case PART:
    case CONTROLLER:
    case GAMESTATE:
    case INVALID:
    default:
        d("[CL] Command 0x%x not implemented\n", command);
        break;
	}

}

void NetClientWorld::processNetwork(float dt) {
	int size;
	NetAddress addr;
	char data[256];

	while ((size = socket.receive(data, sizeof(data))) > 0) {
		NetCommand cmd(data, size);
		parseCommand(cmd);
	}

	if (netTimer.hasEnded() && status == PLAYING) {
		player->control.copy(*control);
		NetCommand* cmdControl = player->control.getCommand();
		socket.send(*cmdControl);
		delete cmdControl;

		netTimer.reset();
	}
}

void NetClientWorld::processPlayer(float dt) {
	if (firstPerson) {
		scene.camera.firstPerson(*player);
	} else {
		scene.camera.thirdPerson(*player, 10.0f);
		scene.camera.y = player->y + 2.0f;
		float ch = terrain.getHeight(scene.camera.x, scene.camera.z);
		if (scene.camera.y < ch) {
			scene.camera.y = ch;
		}
	}
}

void NetClientWorld::processLogic(float dt) {
	GameWorld::processLogic(dt);
    if (status == PLAYING) {
        processPlayer(dt);
    }
}

void NetClientWorld::renderScene(void) {
	if (status == PLAYING) {
		glm::mat4 proj;
		glViewport(0, 0, cfg.screenW, cfg.screenH);
		proj = glm::perspective(45.0f,
			(float)cfg.screenW / (float)cfg.screenH, 0.1f, 1000.0f); 

		scene.render(proj);
		
		SDL_GL_SwapBuffers();
	} else if (status == CONNECTING) {
        if (netTimer.hasEnded()) {
            reconnect();
            netTimer.reset();
        }
		renderConnecting();
	}
}

void NetClientWorld::renderConnecting(void) {
	SurfEffect lastEffect = connectSurf.lastAnimation.effect;
	if (!connectSurf.animating) {
		if (lastEffect == FADE_IN) {
			connectSurf.fadeOut(2000);
		} else {
			connectSurf.fadeIn(1000);
		}
	}

	if (cfg.screenW > cfg.screenH) {
		glViewport((cfg.screenW - cfg.screenH) / 2, 0, cfg.screenH, cfg.screenH);
	} else {
		glViewport(0, (cfg.screenH - cfg.screenW) / 2, cfg.screenW, cfg.screenW);
	}
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	
	connectSurf.render();
	SDL_GL_SwapBuffers();
}

bool NetClientWorld::reconnect() {
    socket.connect(serverAddr);

	std::string nick = cfg.playerName.c_str();
	NetCommand cmd(CONNECT);
	cmd.addArgument(nick);
	
	socket.send(cmd);
	status = CONNECTING;

	return true;
}

bool NetClientWorld::connectTo(NetAddress addr) {
	serverAddr = addr;
	return reconnect();
}

