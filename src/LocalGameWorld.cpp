#include <iostream>
#include <fstream>
#include "LocalGameWorld.hpp"
#include "KeyboardController.hpp"

extern Config cfg;

LocalGameWorld::LocalGameWorld(void) : GameWorld() {
	firstPerson = false;
}

void LocalGameWorld::cleanup(void) {
	clearWorld();
	scene.clean();
	control->clean();
}

void LocalGameWorld::setup() {
	glEnable(GL_DEPTH_TEST);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
    glFrontFace(GL_CCW);
	
	glEnable(GL_BLEND);
	
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

//	SDL_WM_GrabInput(SDL_GRAB_ON);
	SDL_ShowCursor(SDL_DISABLE);

	hud.setup();

}

void LocalGameWorld::loadWorld(const std::string& worldFile) {
	Json::Value root;
	Json::Reader reader;
    std::string worldPath = MAP_PATH + worldFile;

	root = readJSONDoc(worldPath, reader);
	curWorldFile = worldFile;
	
	GameWorld::loadWorld(root);	
	data.state = playing;

	player = Player();
	std::string playerFile = root.get("player", "").asString();
	player.loadPlayer(playerFile);
	player.laps = root.get("laps", 0).asInt();
	scene.addEntity(player);
	
	player.curCheckp = goalCheckpoint;
	player.setLocation(checkpoints[goalCheckpoint]);
	player.setRotation(checkpoints[goalCheckpoint]);
	
	if (control) {
		delete control;
	}
	control = new KeyboardController();
	((KeyboardController*)control)->loadControls(cfg.controls);
}

void LocalGameWorld::processInput(float dt) {
	control->poll();
    
    controlUnit(player, *control, dt);

	if (control->isPressed(shoot)) {
		spawnUnit(player);
	}

	if (control->isPressed(reload)) reloadWorld();
	if (control->isPressed(quit)) running = false;
}

void LocalGameWorld::processLogic(float dt) {
	GameWorld::processLogic(dt);
	processPlayer(dt);
}

void LocalGameWorld::processPlayer(float dt) {
	player.renderLogic(this, dt);

	if ((player.hp <= 0.0f) && data.state == playing) {
		data.state = gameover;	
	};

	if (data.state == playing) {
		if (player.collide(checkpoints[player.curCheckp % nCheckpoints])) {
			if (player.curCheckp % nCheckpoints == 0) {
				player.laps--;
			}
			if (player.laps == 0) data.state = win;
			player.curCheckp = (player.curCheckp + 1) % nCheckpoints; 
			data.checkpoint = true;
			d("%d laps left\n", player.laps);
		}
	}

	if (firstPerson) {
		scene.camera.firstPerson(player);
	} else {
        scene.camera.thirdPerson(player, 50.0f, 180.0f);
		scene.camera.y = player.y + 2.0f;
		float ch = terrain.getHeight(scene.camera.x, scene.camera.z);
		if (scene.camera.y < ch) {
			scene.camera.y = ch;
		}
	}
}

void LocalGameWorld::renderScene(void) {

    glm::mat4 proj;
	glViewport(0, 0, cfg.screenW, cfg.screenH);
	proj = glm::perspective((45.0f + (abs(player.velocity / player.maxSpeed) * 45.0f)),
		(float)cfg.screenW / (float)cfg.screenH, 0.1f, 1000.0f); 

	scene.render(proj);
	
	data.speed = fabsf(player.velocity / player.maxSpeed);
	data.hit = player.hit;
	data.hp = player.hp;
	hud.render(data);

	SDL_GL_SwapBuffers();
}
