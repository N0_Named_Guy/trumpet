#include "Player.hpp"
#include "ModelManager.hpp"
#include "stdafx.h"

extern ModelManager modelManager;
Player::Player() : Unit() {
	gravity = -0.01f;
	friction = -0.005f;

	maxSpeed = 6.0f;
	minSpeed = -1.0f;

	hp = 1.0f;
	laps = 0;
	curCheckp = 0;
}

void Player::loadPlayer(const std::string& file) {
	Json::Reader reader;
	Json::Value root;
	std::string file_ = PLAYER_PATH + file;

	root = readJSONDoc(file_, reader);

	modelPointer = modelManager.getOBJModel(root.get("model","").asString());

	Json::Value physics = root.get("physics", "");
	Json::Value controls = root.get("controls", "");

	gravity = (float)physics.get("gravity", gravity).asDouble();
	friction = (float)physics.get("friction", friction).asDouble();
	maxSpeed = (float)physics.get("maxspeed", maxSpeed).asDouble();
	minSpeed = (float)physics.get("minspeed", minSpeed).asDouble();
	climbAngle = (float)physics.get("climbangle", climbAngle).asDouble() * PIOVER180;
	speedDecayHit = (float)physics.get("speeddecayhit", speedDecayHit).asDouble();
	turnDecay = (float)physics.get("turndecay", turnDecay).asDouble();
	driftFactor = (float)physics.get("driftfactor", driftFactor).asDouble();

	maxTurning = (float)controls.get("maxturning", maxTurning).asDouble();
	turningStep = (float)controls.get("turningstep", turningStep).asDouble();
	accelStep = (float)controls.get("accelstep", accelStep).asDouble();
	brakeStep = (float)controls.get("brakeStep", brakeStep).asDouble();
    jumpVelocity = (float)controls.get("jumpVelocity", jumpVelocity).asDouble();
}

void Player::render(const glm::mat4& proj, const glm::mat4& view) {
	ModelEntity::render(proj, view);
}

void Player::renderLogic(GameWorld* gw, float interpolation) {
	if (hp <= 0.0f) accel = 0.0f;

	Unit::renderLogic(gw, interpolation);
}
