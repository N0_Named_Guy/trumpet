#ifndef POTATO_CONTROLLER_H
#define POTATO_CONTROLLER_H
#pragma once
#include "Entity.hpp"
#include "stdafx.h"

enum ControllerActions {
	left,
	right,
	duck,
    lookUp,
	jump,
	shoot,
	run,

	pause,
	reload,

	slower,
	faster,

	quit
};

const std::string actionsStr[] = {
	"left",
	"right",
	"duck",
    "lookup",
	"jump",
	"shoot",
	"run",

	"pause",
	"reload",
    
    "slower",
    "faster",

	"quit",
	""
};

struct Axis {
	int x;
	int y;
	int z;
};

class Controller {
protected:
	bool* actions;
	int nActions;
	Axis axis;

	std::string getActionString(int action);
	int getActionFromString(std::string& str);

public:
    virtual ~Controller(void);

	virtual void poll(void);
	virtual bool isDown(ControllerActions action);
	virtual bool isPressed(ControllerActions action);
	virtual Axis getAxis(void);
	virtual void resetAxis(void);

	virtual bool press(ControllerActions action);
	virtual bool depress(ControllerActions action);
	virtual bool toggle(ControllerActions action);

	virtual void copy(Controller& control);

	virtual void init(void);
	virtual void clean(void);
};
#endif /* POTATO_CONTROLLER_H */
