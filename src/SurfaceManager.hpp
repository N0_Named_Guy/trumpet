#ifndef POTATO_SURFACE_MANAGER_H
#define POTATO_SURFACE_MANAGER_H
#pragma once

#include "GLSurface.hpp"
#include "stdafx.h"
#include <map>

class SurfaceManager {
private:
	GLuint vbo;
	GLuint ebo;

protected:
	GLuint surfShaderProgram;
	GLuint alphaUnif;
	GLuint colorUnif;

	GLuint projUnif;
	GLuint modelUnif;

	GLuint vao;
	
public:
	SurfaceManager();
	
	void init(void);
	void clean(void);
	GLSurface getSurface(const std::string& filename);
};

#endif /* POTATO_SURFACE_MANAGER_H */
