#include "Scene.hpp"

void Scene::addEntity(Entity& entity) {
	entities.push_back(&entity);
}

void Scene::removeEntity(Entity& entity) {
    entities.remove(&entity);
}

void Scene::render(const glm::mat4& proj) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	
	glm::mat4 view = camera.getView();

	skybox.render(proj, camera);
	
	glEnable(GL_DEPTH_TEST);
	for (std::list<Entity*>::iterator it = entities.begin();
                it != entities.end(); it++) {
					
		(*it)->render(proj, view);
	}
}

void Scene::clean(void) {
	skybox.clean();
}
