#include "Trumpet.hpp"
#include "World.hpp"
#include "IntroWorld.hpp"
#include "GameWorld.hpp"
#include "config.hpp"

#include "ModelManager.hpp"
#include "TextureManager.hpp"
#include "ShaderManager.hpp"
#include "SurfaceManager.hpp"

#include "NetSocket.hpp"
#include "LocalGameWorld.hpp"
#include "NetClientWorld.hpp"
#include "NetServerWorld.hpp"

ModelManager modelManager;
TextureManager textureManager;
ShaderManager shaderManager;
SurfaceManager surfaceManager;

Config cfg;
SDL_Surface* screen;

void setupScreen(void) {
	/* Create a OpenGL screen */
	if ((screen = SDL_SetVideoMode(cfg.screenW, cfg.screenH, 0, SDL_OPENGL | (cfg.fullscreen ? SDL_FULLSCREEN : 0))) == NULL ) {
		d("Unable to create OpenGL screen: %s\n", SDL_GetError());
		SDL_Quit();
		exit(2);
	}

	SDL_WM_SetCaption("TRUMPET", NULL);
	glewInit();

	surfaceManager.init();
}

World* renderWorld(World& w) {
	int t_prev_interp;
	int t_prev_render;

	int t_prev_frames;

    t_prev_frames = t_prev_render = t_prev_interp = SDL_GetTicks();
	float dt;

	float frames = 0;
	w.setup();

	float total_fps = 0;
	int fps_samples = 0;

	while (w.running) {
		
		int t = SDL_GetTicks();
		dt = (float)(t - t_prev_interp) / (float)cfg.gameSpeed;
		if ((t - t_prev_interp) == 0) continue;
		
		w.processInput(dt);
		w.processLogic(dt);

		if (cfg.fps <= 0 || (float)(t - t_prev_render) >= 1000.0f / (float)cfg.fps) {
			w.renderScene();
			t_prev_render = t;
			frames++;
		}

		if (cfg.showFPS && frames >= GAME_SPEED) {
			float cur_fps = (float)frames / (((float)(t - t_prev_frames) / 1000.0f));
			total_fps += cur_fps;
			fps_samples++;

			d("FPS: %.2f [%.2f]\n", total_fps / (float)fps_samples, cur_fps);
			frames = 0;
			t_prev_frames = t;
		}
		t_prev_interp = t;
	}

	return w.nextWorld;
}

void init() {
	if ( SDL_Init(SDL_INIT_VIDEO) < 0 ) {
		d("Unable to initialize SDL: %s\n", SDL_GetError());
		exit(1);
	}

	cfg.load();
	setupScreen();
	net_init();
}

void cleanup() {
	modelManager.freeAllModels();
	textureManager.clearTextures();
	shaderManager.freeAllShaders();
	surfaceManager.clean();
	
	net_cleanup();
	SDL_Quit();
}

int main(int argc, char **argv) 
{ 
	init();

	World* curWorld;
	World* nextWorld;

	srand((unsigned int)time(NULL));

	if (argc >= 2 && argv[1][0] == 's') {
		d("%s\n", "Server mode.");
		SDL_WM_SetCaption("TRUMPET - Server", NULL);
		curWorld = new NetServerWorld();
		((GameWorld*)curWorld)->loadWorld(cfg.initialMap);

	} else if (argc >= 2 && argv[1][0] == 'c') {
		d("%s\n", "Client mode");
		SDL_WM_SetCaption("TRUMPET - Client", NULL);
		curWorld = new NetClientWorld();
		NetAddress addr(cfg.netHost, cfg.netPort);
		((NetClientWorld*)curWorld)->connectTo(addr);

	} else {
		if (!cfg.skipIntro) {
			curWorld = new IntroWorld(cfg.initialMap);
		} else {
			curWorld = new LocalGameWorld();
			((GameWorld*)curWorld)->loadWorld(cfg.initialMap);
		}
	}

	while (curWorld) {
		nextWorld = renderWorld(*curWorld);
		curWorld->cleanup();
		delete curWorld;
		curWorld = nextWorld;
	}

	cleanup();
	return 0;
}
