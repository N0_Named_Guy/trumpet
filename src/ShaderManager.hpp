#ifndef POTATO_SHADER_MANAGER_H
#define POTATO_SHADER_MANAGER_H
#pragma once

#include "stdafx.h"
#include <map>

class ShaderManager {
protected:
	std::map<std::string, GLuint> shaders;
	std::map<std::string, GLuint> programs;
public:
	GLuint getShader(const std::string& filename);
	GLuint getProgram(const std::string& filename);
	void freeAllShaders(void);
	void freeAllPrograms(void);
};

#endif /* POTATO_SHADER_MANAGER_H */
