#ifndef POTATO_TIMER_H
#define POTATO_TIMER_H
#pragma once

#include "stdafx.h"
class Timer {

protected:
	unsigned int startTime;
	unsigned int interval;
public:
	Timer(void);

	virtual void reset(unsigned int pause);
	virtual void reset(void);
	virtual bool stop(void);
	virtual void pause(void);
	virtual bool hasEnded(void);
	virtual unsigned int timeElapsed(void);

};

#endif /* POTATO_TIMER_H */
