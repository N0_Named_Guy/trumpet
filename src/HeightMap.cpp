#include "HeightMap.hpp"

HeightMap::HeightMap(void)
{
}

HeightMap::~HeightMap(void)
{
}

void HeightMap::render(const glm::mat4& proj, const glm::mat4& view) {
	Model::render(proj, view);
}

void HeightMap::loadFromPNG(const std::string& filename,
	const std::string& textureFile,
	float minH, float maxH, float scale, int detail) {
	
	int channels;
	std::string filename_ = MAP_PATH + filename;
	heightMap = SOIL_load_image(
		filename_.c_str(),
		&mWidth, &mHeight, &channels,
		SOIL_LOAD_L
	);

	this->minH = minH;
	this->maxH = maxH;
	this->scale = scale;
	this->detail = detail;

	loadGeometry();

	Material mat;
	mat.tex = textureManager.loadTexture(textureFile, false, 0);

	materials.push_back(mat);
}

void normalSum(Vertex* vertices, int i1, int i2, int i3) {
	GLfloat triangle[3][3];
	GLfloat normal[3];

	// Normal calculation
	triangle[0][0] = vertices[i1].x;
	triangle[0][1] = vertices[i1].y;
	triangle[0][2] = vertices[i1].z;

	triangle[1][0] = vertices[i2].x;
	triangle[1][1] = vertices[i2].y;
	triangle[1][2] = vertices[i2].z;

	triangle[2][0] = vertices[i3].x;
	triangle[2][1] = vertices[i3].y;
	triangle[2][2] = vertices[i3].z;

	calcNormal(triangle, normal);
	vertices[i1].nx += normal[0];
	vertices[i1].ny += normal[1];
	vertices[i1].nz += normal[2];
	vertices[i1].shared++;

	vertices[i2].nx += normal[0];
	vertices[i2].ny += normal[1];
	vertices[i2].nz += normal[2];
	vertices[i2].shared++;

	vertices[i3].nx += normal[0];
	vertices[i3].ny += normal[1];
	vertices[i3].nz += normal[2];
	vertices[i3].shared++;
}

void HeightMap::loadGeometry() {
	int vw = mWidth * detail;
	int vh = mHeight * detail;

	nVertices = vw * vh;
	vertices = new Vertex[nVertices];

	// Make vertice list
	Vertex* v;
	for (int i = 0; i < vw; i++) {
		for (int j = 0; j < vh; j++) {
			v = &vertices[POS(i,j,vw)]; 
			v->x = ((float)i / (float)detail) * scale;	
			v->z = ((float)j / (float)detail) * scale;
			v->y = getHeight(v->x, v->z);

			v->nx = 0.0f;
			v->ny = 0.0f;
			v->nz = 0.0f;
			v->shared = 0;

			v->s = (float)i / (float)vw;
			v->t = (float)j / (float)vh;
		}
	}

	nElements = nVertices * 6;

	for (int i = 0; i < vw - 1; i++) {
		for (int j = 0; j < vh - 1; j++) {
			normalSum(vertices, POS(i, j + 1, vw), POS(i + 1, j, vw), POS(i, j, vw));
			normalSum(vertices, POS(i + 1, j, vw), POS(i, j + 1, vw), POS(i + 1, j + 1, vw));
		}
	}

	for (int i = 0; i < nVertices; i++) {
		vertices[i].nx /= (float)vertices[i].shared;
		vertices[i].ny /= (float)vertices[i].shared;
		vertices[i].nz /= (float)vertices[i].shared;
	}

	elements = makeElementsArray(1, 0, 0, vw, vh);
	buildVBOs();

	delete[] elements;
}

float inline HeightMap::getHeightAt(const int at[2]) {
	return getHeightAt(at[0], at[1]);
}

float inline HeightMap::getHeightAt(int x, int z) {
	return ((heightMap[POS(x,z,mWidth)] / 255.0f) * (maxH - minH)) + minH;
}

float HeightMap::getHeight(float x, float z) {
	int sx = SDL_max(0, SDL_min((int)(x / scale), mWidth - 2)); 
	int sz = SDL_max(0, SDL_min((int)(z / scale), mHeight - 2)); 
	float dx = (fmodf(x, scale)) / scale; 
    float dz = (fmodf(z, scale)) / scale; 
	float topHeight = lerpf(getHeightAt(sx, sz), getHeightAt(sx + 1, sz), dx); 
    float bottomHeight = lerpf(getHeightAt(sx, sz + 1), getHeightAt(sx + 1, sz + 1), dx); 
	float h = lerpf(topHeight, bottomHeight, dz); 

	return h;
}


GLuint* HeightMap::makeElementsArray(int x, int y, int w, int h) {
	return makeElementsArray(1, x, y, w, h);
}

GLuint* HeightMap::makeElementsArray(int step, int x, int y, int w, int h) {
	int nw = w / step;
	int nh = h / step;

	int vw = mWidth * detail;

	int k = 0;
	unsigned int nElem = nw * nh * 6;

	GLuint* elementsArr = new GLuint[nElem];

	for (int i = x; i < (x + nw) - 1; i++) {
		for (int j = y; j < (y + nh) - 1; j++) {
			elementsArr[k++] = POS(i * step, (j + 1) * step, vw);
			elementsArr[k++] = POS((i + 1) * step, j * step, vw);
			elementsArr[k++] = POS(i * step, j * step, vw);

			elementsArr[k++] = POS((i + 1)  * step, j * step, vw);
			elementsArr[k++] = POS(i * step, (j + 1) * step, vw);
			elementsArr[k++] = POS((i + 1) * step, (j + 1) * step, vw);
		}
	}

	return elementsArr;
}
