#include "NetController.hpp"

NetController::NetController(void) {
	SDLKey k[] = CONTROLLER_KEYS;
	nActions = sizeof(k) / sizeof(SDLKey);
	actions = new bool[nActions];
}


NetController::~NetController(void) {
	if (nActions) {
		delete[] actions;
	}
}

NetCommand* NetController::getCommand(void) {
	NetCommand* cmd = new NetCommand(CONTROLLER);
	char* data = new char[nActions];

	for (char i = 0; i < nActions; i++) {
		data[(int)i] = actions[(int)i] ? 255 : 1;	
	}

	cmd->addArgument(data);
	delete data;

	return cmd;
}

void NetController::loadCommand(NetCommand& cmd) {
	if (cmd.getCommand() != CONTROLLER) return;

	const char* data = cmd.getArgument(0);

	for (char i = 0; i < nActions; i++) {
		actions[(int)i] = (data[(int)i] == 1 ? false : true);
	}
}
