#include "GLSurface.hpp"
extern TextureManager textureManager;
extern ShaderManager shaderManager;

GLSurface::GLSurface(const std::string& texture,
		const GLuint vao, const GLuint shader,
		const GLuint proj, const GLuint model,
		const GLuint color) {

	this->vao = vao;
	this->shaderProgram = shader;
	this->projUnif = proj;
	this->modelUnif = model;
	this->colorUnif = color;

	this->tex = textureManager.loadTexture(texture);
	this->lastTicks = SDL_GetTicks();
	this->animTime = 0;
	this->color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	this->animating = false;
	setupTextures();
}

void GLSurface::setupTextures(void) {
	glBindTexture(GL_TEXTURE_2D, tex);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

}

void GLSurface::parseAnim(void) {
	if (!animating || qEffects.empty()) return;

	SurfAnimation curAnim = qEffects.front();
	unsigned int curTime = SDL_GetTicks();

	animTime += curTime - lastTicks;

	switch (curAnim.effect) {
	case FADE_IN: color.a = lerpf(0.0f, 1.0f, ((float)animTime / (float)curAnim.duration)); break;
	case FADE_OUT: color.a = lerpf(1.0f, 0.0f, ((float)animTime / (float)curAnim.duration)); break;
	default: break;
	}

	if (animTime > curAnim.duration) {
		switch (curAnim.effect) {
		case FADE_IN: color.a = 1.0f; break;
		case FADE_OUT: color.a = 0.0f; break;
		default: break;
		}
		lastAnimation = curAnim;
		qEffects.pop_front();
		if (qEffects.empty()) animating = false;
		animTime = 0;
	}

	lastTicks = curTime;

}

void GLSurface::render() {
	render(glm::mat4());
}

void GLSurface::render(const glm::mat4& proj) {
	parseAnim();
	
	// Bind the correct vertex array and use the surface shader program
	glBindVertexArray(vao);
	glUseProgram(shaderProgram);

	// Set projections and transformations
	glm::mat4 model;
	
	model = glm::translate(model, glm::vec3(x, y, z));
	model = glm::rotate(model, roll, glm::vec3(0.0f, 0.0f, 1.0f));
	model = glm::scale(model, glm::vec3(sx, sy, sz));
	
	glUniformMatrix4fv(projUnif, 1, GL_FALSE, glm::value_ptr(proj));
	glUniformMatrix4fv(modelUnif, 1, GL_FALSE, glm::value_ptr(model));

	// Set the surface texture
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, tex);

	// Set the color uniform
	glUniform4fv(colorUnif, 1, glm::value_ptr(color));
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

}

void GLSurface::addAnimation(SurfEffect e, unsigned int ms) {
	if (!animating) {
		animTime = 0;
		lastTicks = SDL_GetTicks();
	}
	SurfAnimation a = {e, ms};
	qEffects.push_back(a);
	animating = true;
}

void GLSurface::stopAnimation(void) {
	qEffects.clear();
	animating = false;
}

void GLSurface::fadeIn(unsigned int ms) {
	addAnimation(FADE_IN, ms);
}

void GLSurface::fadeOut(unsigned int ms) {
	addAnimation(FADE_OUT, ms);
}

void GLSurface::idle(unsigned int ms) {
	addAnimation(IDLE, ms);
}
