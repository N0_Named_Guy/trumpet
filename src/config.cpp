#include "config.hpp"

Config::Config() {
	screenW = SCREEN_W;
	screenH = SCREEN_H;
	gameSpeed = GAME_SPEED;
	fps = FPS;
	fullscreen = FULLSCREEN;
	skipIntro = false;
	initialMap = START_MAP;
	maxInputLatency = minInputLatency = INPUT_LATENCY;
}

Config::~Config() {
}

void Config::load(void) {
	std::string str = CONFIG_PATH;
	load(str);
}

void Config::load(std::string& file) {
	Json::Value root;
	Json::Reader reader;
    
	root = readJSONDoc(file, reader);
	Json::Value resolution = root.get("resolution", "");
	screenW = resolution.get("width", SCREEN_H).asUInt();
	screenH = resolution.get("height", SCREEN_W).asUInt();
	
	Json::Value debug = root.get("debug", "");
	minInputLatency = debug.get("mininputlatency", INPUT_LATENCY).asUInt();
	maxInputLatency = debug.get("maxinputlatency", INPUT_LATENCY).asUInt();
	dropProbability = debug.get("dropprobability", 0.0f).asUInt();

	fullscreen = root.get("fullscreen", FULLSCREEN).asBool();
	playerName = root.get("playername", PLAYER_NAME).asString();
	initialMap = root.get("map", START_MAP).asString();
	skipIntro = root.get("skipintro", false).asBool();
	gameSpeed = root.get("gamespeed", GAME_SPEED).asUInt();
	fps = root.get("fps", FPS).asUInt();
	showFPS = root.get("showfps", SHOW_FPS).asBool();

	netHost = root.get("nethost", NET_HOST).asString();
	netPort = root.get("netport", NET_PORT).asUInt();
	netRate = root.get("netrate", NET_RATE).asUInt();

	controls = root.get("controls", "");
	
}
