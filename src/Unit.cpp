#include "Unit.hpp"

Unit::Unit(void)
{
	accel = velocity = yvelocity = turning = 0.0f;
	
	gravity = 0.01f;
	friction = 0.005f;
	maxSpeed = 6.0f;
	minSpeed = -1.0f;
	climbAngle = 45.0f * PIOVER180;
	speedDecayHit = 1.5f;
	turnDecay = 0.5f;

	maxTurning = 3.0f;
	turningStep = 4.0f;
	accelStep = 0.05f;
	brakeStep = 0.05f;

    jumpVelocity = 10.0f;
    isJumping = true;

	driftFactor = 1.5f;

	hit = false;

	moveState = stopped;
}


Unit::~Unit(void)
{
}

void Unit::turn(float dt) {
	turning += turningStep * dt;
}

void Unit::stopAccel(void) {
	accel = 0.0f;
}

void Unit::accelerate(float dt) {
	accel = accelStep * dt;
	moveState = moving;
}

void Unit::brake(float dt) {
	accel = -brakeStep * dt;
	moveState = moving;
}


bool Unit::checkObjectCollision(GameWorld& gw, float nx, float ny, float nz) {
	for (unsigned int i = 0; i < gw.nScenery; i++) {
		ModelEntity ent = gw.scenery[i];
		if (collide(ent)) {
			return true;
		}
	}
	return false;
}

bool Unit::checkTerrainXZCollision(Terrain& terrain, float ox, float oy, float oz, float nx, float ny, float nz) {
	if (getYDiff(terrain, ox, oy, oz, nx, ny, nz) < 0.0f) return false;
	float angle = getSlope(ox, oy, oz, nx, ny, nz);

	return (angle >= climbAngle);
}

float Unit::getYDiff(Terrain& terrain, float ox, float oy, float oz, float nx, float ny, float nz) {
	float oh = SDL_max(oy, terrain.getHeight(ox, oz));
	float nh = SDL_max(ny, terrain.getHeight(nx, nz));
	return nh - oh;
}

float Unit::getSlope(float ox, float oy, float oz, float nx, float ny, float nz) {
	float b = ny - oy;
	float a = sqrtf(pow(nx - ox, 2.0f) + pow(nz - oz, 2.0f));
	float angle = atan2f(b, a);

	if ((int)(angle / PIOVER180) == 90) angle = 0.0f;

	return angle;
}

float Unit::checkTerrainYCollision(Terrain& terrain, float nx, float ny, float nz) {
	BoundingBox bb = modelPointer->aabb;
	float minH = (bb.maxY - bb.minY) / 2.0f;

	float th = terrain.getHeight(nx, nz) + minH;

	if (y < th && yvelocity < 0.0f) {
		yvelocity = 0.0f;
		y = th;
        if (isJumping) d("%s\n", "Unit on the floor");
        isJumping = false;
		return th;
	}
	return ny;
}

void Unit::setSpeed(float s) {
    moveState = moving; 
	velocity = s;
}

void Unit::renderLogic(GameWorld* gw, float dt) {
	
	float ox, oy, oz;
	float nx, ny, nz;
	BoundingBox bb = modelPointer->aabb;
	
    // Setting variables
	ox = nx = x;
	oy = ny = y;
	oz = nz = z;
	hit = false;

    velocity += accel * dt;
    nx += (sin(-yaw * PIOVER180) * velocity) * dt;
    nz += (-cos(-yaw * PIOVER180) * velocity) * dt;

	yvelocity -= gravity * dt;
	ny += yvelocity * dt;

	if (velocity > maxSpeed) {
		velocity = maxSpeed;
	} else if (velocity < minSpeed) {
		velocity = minSpeed;
	}

	ny = checkTerrainYCollision(gw->terrain, x + bb.minX, ny, z + bb.minZ);

	if (fabsf(turning) > maxTurning) {
		if (turning > 0) turning = maxTurning;
		else turning = -maxTurning;
	}

	float incYaw;
	
	incYaw = (turning * (velocity / maxSpeed)) * dt;
	roll = turning * (velocity / maxSpeed);

	yaw += incYaw;
	
	hit = checkObjectCollision(*gw, nx, ny, nz);
	hit = hit || (
		checkTerrainXZCollision(gw->terrain, ox + bb.minX, oy, oz + bb.minZ, nx + bb.minX, ny, nz + bb.minZ) ||
		checkTerrainXZCollision(gw->terrain, ox + bb.maxX, oy, oz + bb.minZ, nx + bb.maxX, ny, nz + bb.minZ) ||
		checkTerrainXZCollision(gw->terrain, ox + bb.maxX, oy, oz + bb.maxZ, nx + bb.maxX, ny, nz + bb.maxZ) ||
		checkTerrainXZCollision(gw->terrain, ox + bb.minX, oy, oz + bb.maxZ, nx + bb.minX, ny, nz + bb.maxZ)
		);
	
	//hit = hit || checkTerrainXZCollision(gw->terrain, ox, oy, oz, nx, ny, nz);
	
    if (velocity > -friction) {
        velocity -= (friction * dt);
        if (velocity < -friction) { 
            velocity = 0.0f;
            moveState = stopped;
        }
    } else if (velocity < -friction) {
        velocity += (friction * dt);
        if (velocity > -friction) {
            velocity = 0.0f;
            moveState = stopped;
        }
    }

    if (!hit) {
        x = nx;
        y = ny;
        z = nz;
    }
}

void Unit::jump(void) {
    if (isJumping) return;

    // Set initial velocity
    yvelocity = jumpVelocity;

    // Mark it as jumping
    isJumping = true;
}
