#include "Timer.hpp"

Timer::Timer() : startTime(0), interval(0) {}

void Timer::reset(void) {
	startTime = SDL_GetTicks();
}

void Timer::reset(unsigned int interval) {
	this->interval = interval;
	reset();
}

bool Timer::stop() {
	bool ret = hasEnded();

	interval = 0;

	return ret;
}

void Timer::pause() {
	d("[TIMER] %s\n", "DO ME!");
}

bool Timer::hasEnded() {
	return (interval > 0) && (timeElapsed() > interval);
}

unsigned int Timer::timeElapsed(void) {
	return SDL_GetTicks() - startTime;
}
