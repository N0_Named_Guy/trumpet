#ifndef POTATO_UNIT_H
#define POTATO_UNIT_H
#pragma once

#include "ModelEntity.hpp"
#include "Terrain.hpp"

class GameWorld;
#include "GameWorld.hpp"

enum MoveState {
    stopped, moving
};

class Unit : public ModelEntity {
protected:
    bool checkObjectCollision(GameWorld& gw, float nx, float ny, float nz);
    bool checkTerrainXZCollision(Terrain& terrain, float ox, float oy, float oz, float nx, float ny, float nz);
    float getSlope(float ox, float oy, float oz, float nx, float ny, float nz);
    float getYDiff(Terrain& terrain, float ox, float oy, float oz, float nx, float ny, float nz);
    float checkTerrainYCollision(Terrain& terrain, float nx, float ny, float nz);

    MoveState moveState;
public:

    // Physics variables
    float gravity;
    float friction;
    float maxSpeed;
    float minSpeed;
    float climbAngle;
    float speedDecayHit;
    float turnDecay;
    float driftFactor;

    // Physics state
    float accel;
    float velocity;
    float yvelocity;
    float turning;

    // Control variables
    float maxTurning;
    float turningStep;
    float accelStep;
    float brakeStep;

    // Jumping variables
    float jumpVelocity;

    // State variables
    bool isJumping;
    bool hit;

    Unit(void);
    virtual ~Unit(void);
    virtual void turn(float dt);
    virtual void renderLogic(GameWorld* gw, float interpolation);
    virtual void stopAccel(void);
    virtual void accelerate(float a);
    virtual void brake(float a);
    virtual void setSpeed(float s);
    virtual void jump(void);
};

#endif /* POTATO_UNIT_H */
