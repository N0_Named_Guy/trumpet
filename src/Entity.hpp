#ifndef POTATO_ENTITY_H
#define POTATO_ENTITY_H
#pragma once
#include "stdafx.h"

class Entity {

public:
	float x,y,z; // XYZ translation
	//float rx,ry,rz; // XYZ axis rotation
	float sx,sy,sz; // XYZ scale
	
	float pitch, yaw, roll; // Euler rotation

	Entity(void);
	~Entity(void);
	virtual void render(void) {}
	virtual void render(const glm::mat4& proj) {render();}
	virtual void render(const glm::mat4& proj, const glm::mat4& view) {render();}
	virtual void render(const glm::mat4& proj, const glm::mat4& view, const glm::mat4& model) {render();}

	void setLocation(Entity& ent);
	void setLocation(float x, float y, float z);
	//void setRotation(Entity& ent);
	//void setRotation(float rx, float ry, float rz);
	void setRotation(Entity& ent);
	void setRotation(float pitch, float yaw, float roll);
    void setPitch(float pitch);
    void setYaw(float yaw);
    void setRoll(float roll);

	void setScale(Entity& ent);
	void setScale(float sx, float sy, float sz);

	void facePoint(float x, float y, float z);
	void turnRight(float angle);
	void turnUp(float angle);
	void forward(float step, float direction);
	void forward(float step);
	void strafe(float step);

};

#endif /* POTATO_ENTITY_H */
