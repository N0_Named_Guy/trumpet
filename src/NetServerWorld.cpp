#include "NetServerWorld.hpp"
#include "KeyboardController.hpp"

extern Config cfg;

NetServerWorld::NetServerWorld(void)
{
	nextPlayerId = 0;
}

void NetServerWorld::setup(void) {
	if (!socket.listen(cfg.netPort)) {
        d("[SV] %s\n", "Couldn't open listen socket");
        running = false;
    }
    d("[SV] %s\n", "Waiting for incoming connections");
	
	control = new KeyboardController();
	((KeyboardController*)control)->loadControls(cfg.controls);

	SDL_ShowCursor(SDL_DISABLE);
	glEnable(GL_DEPTH_TEST);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);
	
	glEnable(GL_BLEND);
	
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void NetServerWorld::cleanup(void) {
    std::unordered_map<NetAddress, NetPlayer*, NetAddressHasher>::iterator it;

    d("There are %lu players\n", players.size());

    while (players.size() > 0) {
        it = players.begin();
        removePlayer(it->first);
    }

	control->clean();

	delete control;
}

void NetServerWorld::processLogic(float dt) {
	scene.camera.y = terrain.getHeight(scene.camera.x, scene.camera.z) + 10.0f;
	GameWorld::processLogic(dt);
}

void NetServerWorld::loadWorld(const std::string& world) {
	GameWorld::loadWorld(world);
}

char NetServerWorld::createPlayer(const std::string& name, const NetAddress& addr) {
	char id = nextPlayerId++;
	NetPlayer* p = new NetPlayer(name, addr, id);
	p->loadPlayer("ship.json");

	p->setLocation(checkpoints[0]);
	p->setRotation(checkpoints[0]);

	p->curCheckp = goalCheckpoint;

    players[addr] = p;

	return id;
}

NetPlayer* NetServerWorld::getPlayerByAddr(const NetAddress& addr) {
    if (players.count(addr) == 1) {
        return players[addr];
    } else {
        return NULL;
    }
}

void NetServerWorld::connectPlayer(NetCommand& cmd, const NetAddress& addr) {
    NetCommand toSend(ACCEPT);

    if (players.count(addr) != 0) {
        return;
    }

    char* name = cmd.getArgument((char)0);

    d("[SV] %s has connected [%d.%d.%d.%d]\n", name,
        addr.getA(), addr.getB(), addr.getC(), addr.getD());

    toSend.addArgument(name);
    toSend.addArgument(createPlayer(name, addr));
    d("[SV] Sending ACCEPT command to %s\n", name);
    socket.send(addr, toSend);
    
    NetCommand toSend2 = NetCommand(LEVEL);
    toSend2.addArgument(curWorldFile);
    d("[SV] Sending LEVEL command (%s)\n", curWorldFile.c_str());
    socket.send(addr, toSend2);
}

void NetServerWorld::readyPlayer(const NetAddress& addr) {
    NetPlayer* p = getPlayerByAddr(addr);

    d("[SV] Player %s (%d.%d.%d.%d:%d) is ready\n", p->name.c_str(),
            addr.getA(), addr.getB(), addr.getC(), addr.getD(),
            addr.getPort());
    p->ready = true;

	units.push_back(p);
	scene.addEntity(*p);
}

void NetServerWorld::removePlayer(const NetAddress& addr) {
    NetPlayer* p = getPlayerByAddr(addr);

    if (p == NULL) {
        d("[SV] %s\n", "Bogus quit command");
        return;
    }

    NetCommand cmd(QUIT);
    socket.send(addr, cmd);

    d("%s has quit (%d.%d.%d.%d:%d)\n", p->name.c_str(),
        addr.getA(), addr.getB(), addr.getC(), addr.getD(),
        addr.getPort());

    scene.removeEntity(*p);
    players.erase(addr);
    
    //delete p;
}

void NetServerWorld::updatePlayer(NetCommand& cmd, const NetAddress& addr) {
    NetPlayer* p = getPlayerByAddr(addr);
    if (p == NULL) {
        return;
    }

    p->control.loadCommand(cmd);
}

void NetServerWorld::parseCommand(NetCommand& cmd, NetAddress& addr) {
    char command = cmd.getCommand();

	switch (command) {
	case CONNECT:
        connectPlayer(cmd, addr);
		break;

    case READY:
        readyPlayer(addr);
        break;

    case QUIT:
        removePlayer(addr);
        break;

    case CONTROLLER:
        updatePlayer(cmd, addr);
        break;

    case PING:
    case PONG:
    case LEVEL:
    case JOIN:
    case PART:
    case GAMESTATE:
    case INVALID:
    default:
        d("[SV] Command 0x%x not implemented\n", command);
        break;
	}

}

void NetServerWorld::processNetwork(float dt) {
	NetAddress recvAddr;
	char data[256];
	
	int size;
	while ((size = socket.receive(recvAddr, data, sizeof(data))) > 0) {
		NetCommand cmd(data, size);
		parseCommand(cmd, recvAddr);
	}
}

void NetServerWorld::processPlayers(float dt) {
    std::unordered_map<NetAddress, NetPlayer*, NetAddressHasher>::iterator it;

    for (it = players.begin(); it != players.end(); ++it) {
        NetPlayer* p = it->second;
        GameWorld::controlUnit(*p, p->control, dt);
    }
}

void NetServerWorld::processInput(float dt) {
	control->poll();

	if (control->isDown(lookUp)) {
		scene.camera.forward(dt * 4.0f);
	} else if (control->isDown(duck)) {
		scene.camera.forward(-dt * 4.0f);
	}

	if (control->isDown(left)) {
		scene.camera.strafe(-dt * 4.0f);
	} else if (control->isDown(right)) {
		scene.camera.strafe(dt * 4.0f);
	}

	if (control->isPressed(quit)) {
		running = false;
	}

	if (control->isPressed(run)) {
		mouseView = !mouseView;
		if (mouseView) {
			SDL_WM_GrabInput(SDL_GRAB_ON);
			SDL_ShowCursor(SDL_DISABLE);
		} else {
			SDL_WM_GrabInput(SDL_GRAB_OFF);
			SDL_ShowCursor(SDL_ENABLE);
		}
	}

	if (mouseView) {
		Axis axis = control->getAxis();
		scene.camera.turnUp((float)axis.y);
		scene.camera.turnRight((float)axis.x);
		control->resetAxis();
	}

	processNetwork(dt);
    processPlayers(dt);

}

void NetServerWorld::renderScene() {

	glm::mat4 proj;
	glViewport(0, 0, cfg.screenW, cfg.screenH);
	proj = glm::perspective(45.0f,
		(float)cfg.screenW / (float)cfg.screenH, 0.1f, 1000.0f); 

	scene.render(proj);

	SDL_GL_SwapBuffers();
}
