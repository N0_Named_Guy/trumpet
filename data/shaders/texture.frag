#version 130

precision mediump float;

in vec2 Texcoord;
in vec4 Color;

out vec4 outColor;

uniform sampler2D texture;

void main() {
    outColor = Color * texture2D(texture, Texcoord);
}
