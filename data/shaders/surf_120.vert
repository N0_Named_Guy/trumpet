#version 120

attribute vec2 position;
attribute vec2 texCoord;

varying vec2 TexCoord;

uniform mat4 model;
uniform mat4 proj;

void main()
{
    gl_Position = proj * model * vec4(position, 0.0, 1.0);
    TexCoord = texCoord;
}