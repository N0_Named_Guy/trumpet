#version 120

varying vec2 TexCoord;

uniform vec4 color;
uniform sampler2D surfTex;

void main()
{
    gl_FragColor = texture2D(surfTex, TexCoord) * color;
}