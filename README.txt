Trumpet Project - codenamed Kuckflluks

This is an in-development game, made by
David Serrano aka N0_Named_Guy.

TABLE OF CONTENTS:
* Before you run
* Configuration
* Controls
* Mapping and Modelling
* Linux binary
* Contact

== Before you run ==

Ensure you have the Visual C++ 2010 runtime libraries
installed on your machine. Here's links for the lazy:

* For Windows XP/Vista/7 32 bits
http://www.microsoft.com/download/en/details.aspx?id=5555 

* For Windows XP/Vista/7 64 bits
http://www.microsoft.com/download/en/details.aspx?id=14632 

If after installing the runtime, you still can't run the game,
make sure you also extracted the DLL files, as well as the
data directory. At this stage, if any file is missing in
the data folder, unexpected behaviour will occur (your
computer will be fine, don't worry).

By the way, to run the game, you just have to double click
the trumpet.exe file (but you already knew that, didn't you?).

== Configuration ==

Edit the config.json file, to suit your needs. I am in no way
responsible if you break your graphics card by setting an invalid
fullscreen resolution. You have been warned!

== Controls ==

L/R Arrows - Rotate the ship
U/D Arrows - Speeds up/down the ship
ESC - Quit the game

== Mapping and Modelling ==

Mapping is easy. Go inside the data/maps folder, and hack away.
The map's textures are located at data/textures. Change stuff,
see what happens.

== Linux Binary ==

Yes! This game also runs in Linux. The game runs fine using wine,
but if you wish a native binary for Linux  DM me at @n0namedguy.

== Contact ==

You can contact me over my twitter account @n0namedguy.

